<?php

add_action('init', function() {
	add_rewrite_endpoint('videos', EP_ROOT | EP_PAGES);
});
add_filter('woocommerce_account_menu_items', function($items) {
	$items['videos'] = __('Videos');
	return $items;
});
add_action('woocommerce_account_videos_endpoint', function() {
    $user_id = get_current_user_id();
    $order_statuses = array('wc-processing', 'wc-completed');

    $customer_orders = wc_get_orders( array(
        'meta_key'      => '_customer_user',
        'meta_value'    => $user_id,
        'post_status'   => $order_statuses,
        'numberposts'   => -1,
        'return'        => 'ids'
    ) );
    if(count($customer_orders)>0){
        $videos = get_option( 'input_videos_settings' );
        $videos = json_decode($videos, true);
        $array_search       = array("[" , "]");
        $array_replace      = array("<" , ">");
        for ($i=0; $i < count($videos); $i++) { 
            switch ($videos[$i]['type']) {
                case 'url':
                    ?>
                    <video src="<?=$videos[$i]['content']?>"></video>
                    <?php
                    break;
                case 'code':
                    echo str_replace($array_search , $array_replace , $videos[$i]['content']);
                    break;
            }
        }
    }else{
        ?>
        <h4>
            Debe hacer una compra para ver los videos
        </h4>
        <?php
    }
});