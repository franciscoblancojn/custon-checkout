<?php


require_once plugin_dir_path( __FILE__ ) . 'functions.php';
require_once plugin_dir_path( __FILE__ ) . 'option_page.php';
require_once plugin_dir_path( __FILE__ ) . 'widget.php';
require_once plugin_dir_path( __FILE__ ) . 'endpoint.php';
require_once plugin_dir_path( __FILE__ ) . 'videos.php';
require_once plugin_dir_path( __FILE__ ) . 'hook.php';