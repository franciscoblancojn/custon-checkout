<?php
add_control_style( "button" , ".btn_colla" , $this , array(
    'hover' => true,
    'padding' => true,
    'border' => true,
    'color' => true,
    'background' => true,
    'typography' => true,
    'margin' => true,
) , 'Button Collapse');
add_control_style( "collapse" , ".content_collase" , $this , array(
    'hover' => true,
    'padding' => true,
    'border' => true,
    'background' => true,
    'margin' => true,
) , 'Collapse');
add_control_style( "title" , ".title" , $this , array(
    'padding' => true,
    'border' => true,
    'margin' => true,
    'typography' => true,
    'color' => true,
) , 'Title Question');
add_control_style( "select" , ".select" , $this , array(
    'padding' => true,
    'border' => true,
    'margin' => true,
    'typography' => true,
    'color' => true,
    'background' => true,
    'hover' => true,
) , 'Select Question');
add_control_style( "buttons" , ".navigation-cuestionario-button" , $this , array(
    'hover' => true,
    'padding' => true,
    'border' => true,
    'color' => true,
    'background' => true,
    'typography' => true,
    'margin' => true,
) , 'Navigation buttons');
add_control_style( "submit" , ".submit" , $this , array(
    'hover' => true,
    'padding' => true,
    'border' => true,
    'color' => true,
    'background' => true,
    'typography' => true,
    'margin' => true,
) , 'Button Submit');
