<?php
add_control_style( "button" , "button" , $this , array(
    'hover' => true,
    'padding' => true,
    'border' => true,
    'color' => true,
    'background' => true,
    'typography' => true,
    'margin' => true,
) , 'Button');