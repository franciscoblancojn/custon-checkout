<?php
add_action( 'admin_menu', 'option_page_videos' );
 
function option_page_videos() {
	add_menu_page(
		'Videos', // page <title>Title</title>
		'Videos', // menu link text
		'manage_options', // capability to access the page
		'videos-slug', // page URL slug
		'admin_videos', // callback function /w content
		'dashicons-video-alt', // menu icon
		5.1 // priority
	);
 
}
add_action( 'admin_init',  'videos_register_setting' );

function videos_register_setting(){
 
	register_setting(
		'videos_settings', // settings group name
		'input_videos_settings', // option name
		'sanitize_text_field' // sanitization function
	);
 
	add_settings_section(
		'videos_settings_section_id', // section ID
		'', // title (if needed)
		'', // callback function (if needed)
		'videos-slug' // page slug
	);
 
	add_settings_field(
		'input_videos_settings',
		'input_videos_settings',
		'videos_text_field_html', // function which prints the field
		'videos-slug', // page slug
		'videos_settings_section_id', // section ID
		array( 
			'label_for' => 'input_videos_settings',
			'class' => 'misha-class', // for <tr> element
		)
    );
 
}
function videos_text_field_html(){
 
	$text = get_option( 'input_videos_settings' );
 
	printf(
        '<textarea 
        id="input_videos_settings" 
        name="input_videos_settings"
        >%s</textarea>',
		esc_attr( $text )
	);
 
}

function admin_videos(){
    ?>
    <div class="wrap">
	    <h1>Videos</h1>
        <div id="content_videos">
        </div>
        <div id="add_video">
            Agregar Video por medio de:
            <br>
            <br>
            <button 
            class="button button-primary" 
            onclick="add_video({'type':'url','content':''})">
                Url
            </button>
            <button 
            class="button button-primary" 
            onclick="add_video({'type':'code','content':''})">
                Codigo
            </button>
            <br>
            <br>
        </div>
	    <form method="post" action="options.php">
            <?php
                settings_fields( 'videos_settings' ); 
                do_settings_sections( 'videos-slug' ); 
                submit_button();
            ?>
	    </form>
        <style>
            .button.button-primary{
                min-width:100px;
            }
            .video{
                margin-bottom:25px;
            }
            .video_input{
                width:500px;
                margin:0;
            }
            .misha-class{
                display:none;
            }
        </style>
        <script>
            content_videos  = document.getElementById('content_videos')
            input           = document.getElementById('input_videos_settings')
            submit           = document.getElementById('submit')
            submit.onclick = function(){
                save_videos()
            }
            function add_video(e){
                new_video = document.createElement('div')
                new_video.classList.add('video')
                e.content = e.content.split("[").join("<").split("]").join(">")
                switch (e.type) {
                    case 'url':
                        new_video.innerHTML = `
                            <input
                            type="text"
                            value="${e.content}"
                            class="video_url video_input"
                            placeholder="Url"
                            data-type="url"
                            />
                        `
                        break;
                    case 'code':
                        new_video.innerHTML = `
                            <textarea
                            class="video_code video_input"
                            placeholder="Code"
                            data-type="code"
                            >${e.content}</textarea>
                        `
                        break;
                }
                new_video.innerHTML+=`
                    <button 
                    class="button button-primary" 
                    onclick="delete_video(this)">
                        Eliminar
                    </button>
                `
                content_videos.appendChild(new_video)
            }
            function delete_video(e){
                e.parentElement.outerHTML = ""
            }
            function save_videos(){
                e = document.documentElement.querySelectorAll('.video_input')
                value = []
                for (i = 0; i < e.length; i++) {
                    content = e[i].value.split("<").join("[").split(">").join("]")
                    value[i] = {
                        type : e[i].getAttribute("data-type"),
                        content : `${content}`
                    }
                }
                value = JSON.stringify(value)
                input.value = value
            }
            function load_videos(){
                value = input.value
                if(value == ""){
                    value = "[]"
                }
                value = JSON.parse(value)
                for (i = 0; i < value.length; i++) {
                    add_video(value[i])
                }
            }
            load_videos()

        </script>
    </div>
    <?php
}