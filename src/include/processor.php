<?php
require_once(preg_replace('/wp-content.*$/','',__DIR__).'wp-load.php');

$tracto_digestivo = (isset($_POST['tracto_digestivo']))?$_POST['tracto_digestivo']:"off";

$count = 0;
foreach ($_POST['question'] as $key => $value) {
    $count += intval($value);
}
$product = get_option( 'input_custionario_settings_products' );
$product = json_decode($product , true);
$product_id = -1;
/**
 * Producto_2
 * Si adicionalmente a este cuestionario tienes una condición autoinmune (tiroiditis autoinmune o de Hashimoto, enfermedad de Graves, Artritis reumatoide, Colitis ulcerativa, síndrome de Sjogren, Lupus eritematoso sistémico, Vitíligo, Morbus Crohn, etc.) te recomendamos seguir el Programa de recuperación inmune. Haz clic aquí para más información.
 */
if($_POST['autoinmune'] != "no"){
    $url = wc_get_checkout_url()."?add-to-cart=".$product['select_product_2'];
    $product_id = $product['select_product_2'];
}else
/**
 * Producto_1
 * Si tu puntaje es mayor a 30 o la mayoría de tus síntomas se concentran en el tracto digestivo te recomendamos seguir una dieta de eliminación. Haz clic aquí para mayor información.
 * 
 */
if($count > 30 || $tracto_digestivo=="on"){
    $url = wc_get_checkout_url()."?add-to-cart=".$product['select_product_1'];
    $product_id = $product['select_product_1'];
}else{
    /**
     * No Tratamiento
     */
    
    $url = get_option( 'input_custionario_settings_url' );
}

$user_id = get_current_user_id();
update_user_meta($user_id,'puntuacion',$count);


get_header('shop');
?>
<div class="">
    <div class="container_pp">
        <h1>
            Gracias por terminar el cuestionario
        </h1>
        <h2>
            Tu puntuacion es:
            <br>
            <strong>
                <?=$count?>
            </strong>
        </h2>
        <?php
            if($product_id == -1){
                ?>
                <p>
                    No necesitas un Tratamiento
                    <br>
                    <a href="<?=$url?>">Segir navegando</a>
                    </p>
                <?php
            }else{
                $product = wc_get_product( $product_id );
                ?>
                <p>
                    Te recomendamos
                    <br>
                    <a href="<?=$url?>" class="productName"> 
                        <strong>
                            <?=$product->get_name()?>
                        </strong>
                    </a>
                </p>
                <?php
            }
        ?>
    </div>
    <style>
        .container_pp{
            margin:20px 0;
            padding: 10px;
            width: 100%;
            text-align:center;
        }
        .productName{
            font-size: 40px;
            text-transform: uppercase;
            background-color: #87cabf;
            border-radius: 45px;
            font-family: "Roboto", Sans-serif;
            letter-spacing: 0.6px;
            color: #FFFFFF;
            border-style: solid;
            border-width: 0px 0px 0px 0px;
            border-color: #53444400;
            padding: 20px;
            line-height: 1;
            display: inline-block;
            margin-top: 10px;
            transition: 300ms;
        }
        .productName:hover{
            color:#fff;
            opacity: 0.8;
        }

        .elementor-element-75e29fce{
            background: #87cabf !important;
        }
        @media (max-width:575px){
            .productName{
                font-size:20px;
            }
        }
    </style>
</div>
<?php
get_footer();