<?php

/**
 * Template for widget Elementor for CUestionario
 */
$user_id = get_current_user_id();
if($user_id == 0){
    ?>
    <script>
        window.location = "/my-account/"
    </script>
    <?php
    exit;
}

$value = get_option('input_custionario_settings');
$value = json_decode($value, true);
$pasos = 1;
?>
<form action="<?= plugin_dir_url(__FILE__) ?>processor.php" method="POST">
    <?php
    for ($i = 0; $i < count($value); $i++) {
        $before = "</div>";
        if ($i == 0) {
            $before == "";
        }
        if ($i % 5 == 0) {
            echo $before;
    ?>
            <label class="btn_colla" for="content_collase_<?= $i ?>">
                Paso <?= $pasos++ ?>
            </label>
            <input class="checkbox_colla" type="checkbox" id="content_collase_<?= $i ?>" name="content_collase_<?= $i ?>">
            <div class="content_collase">
            <?php
        }
            ?>
            <div class="question_styles_re" id="question_<?= $i ?>" >
                <h4 class="title">
                    <?= $value[$i]['question'] ?>
                </h4>
                <select id="option_<?= $i ?>" name="question[]" required class="select">
                    <option value="" selected="true" disabled="disabled">Seleccione</option>
                    <option value="0">AUSENTES</option>
                    <option value="1">OCASIONALES y LEVES</option>
                    <option value="2">OCASIONALES pero SEVEROS</option>
                    <option value="3">FRECUENTES y LEVES</option>
                    <option value="4">FRECUENTES y SEVEROS</option>
                </select>
            </div>
            <button id="next_question_button">Siguiente</button>
        <?php
    }
        ?>
            </div>
            <label class="btn_colla" for="content_collase_validation">
                Paso <?= $pasos++ ?>
            </label>
            <input class="checkbox_colla" type="checkbox" id="content_collase_validation" name="content_collase_validation">
            <div class="content_collase last-paso">
                <div class="last-select" id="validation 3">
                    <h4 class="title">
                        Tienes una condición autoinmune
                    </h4>
                    <select name="autoinmune" id="autoinmune" required class="select">
                        <option value="" selected="true" disabled="disabled">Seleccione</option>
                        <option value="no">No</option>
                        <option value="tiroiditis autoinmune o de Hashimoto">Tiroiditis autoinmune o de Hashimoto</option>
                        <option value="enfermedad de Graves">Enfermedad de Graves</option>
                        <option value="Artritis reumatoide">Artritis reumatoide</option>
                        <option value="Colitis ulcerativa">Colitis ulcerativa</option>
                        <option value="síndrome de Sjogren">Síndrome de Sjogren</option>
                        <option value="Lupus eritematoso sistémico">Lupus eritematoso sistémico</option>
                        <option value="Vitíligo">Vitíligo</option>
                        <option value="Morbus Crohn">Morbus Crohn</option>
                        <option value="otro">Otro</option>
                    </select>
                </div>
                <div class="check-form-er" id="validation 2">
                    <label for="tracto_digestivo" class="title">
                        La mayoría de tus síntomas se concentran en el tracto digestivo
                    </label>
                    <input type="checkbox" id="tracto_digestivo" name="tracto_digestivo">
                </div>
                <div class="check-form-er" id="validation 4">
                    <label for="hipotiroidismo_sobrepeso" class="title">
                        Tienes hipotiroidismo o tienes problemas de sobrepeso y te cuesta perderlo con dietas convencionales
                    </label>
                    <input type="checkbox" id="hipotiroidismo_sobrepeso" name="hipotiroidismo_sobrepeso">
                </div>
            </div>
            <input type="submit" class="submit">
</form>
<style>
    /**ajustes collase */
    .content_collase {
        overflow: hidden;
        position: relative;
        visibility: visible;

    }

    .checkbox_colla {
        display: none;
    }

    .checkbox_colla:not(:checked)+.content_collase {
        height: 0;
        padding: 0 !important;
        margin: 0 !important;
        border: 0 !important;
    }

    .btn_colla {
        cursor: pointer;
        display: block;
        width: 100%;
    }

    .question_styles_re {
        display: flex;
        justify-content: space-between;
        position: relative;
    }

    .question_styles_re .title {
        display: inline-block;
    }

    .question_styles_re select {
        width: 380px;
    }



    .question_styles_re:after {
        content: '';
        position: absolute;
        width: 100%;
        height: 1px;
        bottom: 0;
        background-color: #A19BCB;
        margin: 0 auto;
        left: 0;
        right: 0;
    }

    .question_styles_re:last-child:after {
        content: none;
    }

    .last-paso div {
        border-bottom: 1px solid #A19BCB;
    }

    .last-select {
        display: flex;
        align-items: center;
        justify-content: space-between;
    }

    .last-select #autoinmune {
        width: 380px;
        max-height: 32px;
    }

    .check-form-er {
        display: flex;
        align-items: center;
        justify-content: space-between;
    }

    .check-form-er .title {
        max-width: 800px;
    }
</style>
<script>

    var page = 0;
    const buttonNext = document.querySelector('#next_question_button')
    const labels =  document.querySelectorAll('.btn_colla')

    buttonNext.addEventListener('click',()=>{
        page = (page + 1) * 5
        showPage()
    })
    showPage

    showPage(){
        labels.forEach(val=>{
            const attrNumber = Number(val.getAttribute('for').split('_')[2])
            if(attrNumber == page){
                val.style.display = 'block'
            } else{
                val.style.display = 'none'

            }
        })
    }
</script>
<?php


// echo "<pre>";
// var_dump($value);
// echo "</pre>";