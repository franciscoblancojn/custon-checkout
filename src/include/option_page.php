<?php
add_action( 'admin_menu', 'option_page_customCheckout' );
 
function option_page_customCheckout() {
	add_menu_page(
		'Cuestionario', // page <title>Title</title>
		'Cuestionario', // menu link text
		'manage_options', // capability to access the page
		'custionario-slug', // page URL slug
		'admin_cuestionario', // callback function /w content
		'dashicons-welcome-write-blog', // menu icon
		5 // priority
	);
 
}
add_action( 'admin_init',  'misha_register_setting' );
 
function misha_register_setting(){
 
	register_setting(
		'custionario_settings', // settings group name
		'input_custionario_settings', // option name
		'sanitize_text_field' // sanitization function
	);
 
	add_settings_section(
		'custionario_settings_section_id', // section ID
		'', // title (if needed)
		'', // callback function (if needed)
		'custionario-slug' // page slug
	);
 
	add_settings_field(
		'input_custionario_settings',
		'input_custionario_settings',
		'custionario_text_field_html', // function which prints the field
		'custionario-slug', // page slug
		'custionario_settings_section_id', // section ID
		array( 
			'label_for' => 'input_custionario_settings',
			'class' => 'misha-class', // for <tr> element
		)
    );
 
	register_setting(
		'custionario_settings', // settings group name
		'input_custionario_settings_products', // option name
		'sanitize_text_field' // sanitization function
	);
	add_settings_field(
		'input_custionario_settings_products',
		'input_custionario_settings_products',
		'custionario_text_field_html_products', // function which prints the field
		'custionario-slug', // page slug
		'custionario_settings_section_id', // section ID
		array( 
			'label_for' => 'input_custionario_settings_products',
			'class' => 'misha-class', // for <tr> element
		)
    );
    
	register_setting(
		'custionario_settings', // settings group name
		'input_custionario_settings_url', // option name
		'sanitize_text_field' // sanitization function
	);
	add_settings_field(
		'input_custionario_settings_url',
		'Url en caso de no requerir tratamiento',
		'custionario_text_field_html_url', // function which prints the field
		'custionario-slug', // page slug
		'custionario_settings_section_id', // section ID
		array( 
			'label_for' => 'input_custionario_settings_url',
		)
	);
 
}
function custionario_text_field_html(){
 
	$text = get_option( 'input_custionario_settings' );
 
	printf(
		'<input type="text" id="input_custionario_settings" name="input_custionario_settings" value="%s" />',
		esc_attr( $text )
	);
 
}
function custionario_text_field_html_products(){
 
	$text = get_option( 'input_custionario_settings_products' );
 
	printf(
		'<input type="text" id="input_custionario_settings_products" name="input_custionario_settings_products" value="%s" />',
		esc_attr( $text )
	);
 
}
function custionario_text_field_html_url(){
 
	$text = get_option( 'input_custionario_settings_url' );
 
	printf(
		'<input type="text" id="input_custionario_settings_url" name="input_custionario_settings_url" value="%s" />',
		esc_attr( $text )
	);
 
}
function html_select($n = 1){
    $products =  get_posts( array(
        'post_type' => 'product',
        'numberposts' => -1,
        'post_status' => 'publish',
        'fields' => 'ids',
    ));
    $select = "<select 
            name='select_product_".$n."'
            id='select_product_".$n."'
            requiered
            onchange='selectProduct(this)'
            />
            <option 
            value='' 
            selected='true' 
            disabled='disabled'>
                Seleccione
            </option>
        ";
    for ($i=0; $i < count($products); $i++) { 
        $product_id = $products[$i];
        $product = wc_get_product( $product_id );
        $select .= "
            <option value='".$product_id."'>
                ".$product->get_name()."
            </option>
        ";
    }
    $select .= "</select>";
    return $select;
}
function admin_cuestionario(){
    ?>
    <div class="wrap">
	    <h1>Cuestionario</h1>
        <div id="content_c">
            <div class="th">
                <div class="question_th">Pregunta</div>
            </div>
            <div id="fields"></div>
            <button 
            id="btn_add"
            class="button button-primary"
            >
                Agregar Campo
            </button>
        </div>
        <h1>Productos</h1>
        <div id="prodcutos">
            <div class="product_1">
                Si tu puntaje es mayor a 30 o la mayoría de tus síntomas se concentran en el tracto digestivo te recomendamos seguir una dieta de eliminación
                <br>
                <br>
                <?=html_select(1);?>
                <br>
                <br>
                <br>
            </div>
            <div class="product_2">
                Si adicionalmente a este cuestionario tienes una condición autoinmune (tiroiditis autoinmune o de Hashimoto, enfermedad de Graves, Artritis reumatoide, Colitis ulcerativa, síndrome de Sjogren, Lupus eritematoso sistémico, Vitíligo, Morbus Crohn, etc.) te recomendamos seguir el Programa de recuperación inmune. 
                <br>
                <br>
                <?=html_select(2);?>
                <br>
                <br>
                <br>
            </div>
        </div>
	    <form method="post" action="options.php">
            <?php
                settings_fields( 'custionario_settings' ); 
                do_settings_sections( 'custionario-slug' ); 
                submit_button();
            ?>
	    </form>
        <style>
            #fields{
                margin: 20px 0;
            }
            .misha-class{
                display:none;
            }
            .question_th,
            .question{
                width: 700px;
                display: inline-block;
            }
            .min_max_th,
            .min,.max{
                width: 40px;
                display: inline-block;
            }
            .th > div{
                text-transform:uppercase;
                font-weight:bold;
            }
            .field{
                margin-bottom:15px;
            }
        </style>
        <script>
            input_custionario_settings  = document.getElementById('input_custionario_settings')
            fields  = document.getElementById('fields')
            btn_add = document.getElementById('btn_add')
            submit = document.getElementById('submit')
            
            btn_add.onclick = function(){
                add_field()
            }
            submit.onclick = function(){
                save_fields()
            }
            function add_field(value = null){
                dafault = {
                    question    : "",
                }
                if(value != null)[
                    dafault = value
                ]
                ele = document.createElement('div')
                ele.classList.add('field')
                ele.innerHTML = `
                    <input 
                    type="text" 
                    placeholder="Pregunta?" 
                    class="question" 
                    onchange="save_field(this)"
                    value="${dafault.question}"
                    />
                    <button 
                    onclick="delete_field(this)"
                    class="button button-primary"
                    >
                        Eliminar
                    </button>
                `
                ele.field = dafault
                fields.appendChild(ele)
            }
            function delete_field(e){
                e.parentElement.outerHTML = ""
                save_fields()
            }
            function save_field(e){
                e.parentElement.field[e.classList.value] = e.value
            }
            function save_fields(){
                fields_inputs = document.documentElement.querySelectorAll('.field')
                filed_input = []
                for (i = 0; i < fields_inputs.length; i++) {
                    filed_input[i] = fields_inputs[i].field;
                }
                input_custionario_settings.value = JSON.stringify(filed_input)
            }
            function load_value(){
                value = input_custionario_settings.value
                if(value == ""){
                    value = []
                }
                value = JSON.parse(value)
                for ( i = 0; i < value.length; i++) {
                    add_field(value[i])
                }
            }
            load_value()

            //select Product
            input_select = document.getElementById('input_custionario_settings_products')

            var selectProduct_value;
            function selectProduct(e){
                selectProduct_value[e.id] = e.value
                input_select.value = JSON.stringify(selectProduct_value)
            }
            function load_selectProduct(){
                if(input_select.value == ""){
                    input_select.value = `
                        {
                            "select_product_1" : "",
                            "select_product_2" : ""
                        }
                    `
                }
                selectProduct_value = JSON.parse(input_select.value)
                for (const [key, value] of Object.entries(selectProduct_value)) {
                    document.getElementById(key).value = value
                }
            }
            load_selectProduct()
        </script>
    </div>
    <?php
}