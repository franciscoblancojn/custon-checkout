<?php

/**
 * Limitar numero de productos en el carrito
 */
function DRCP_limitarNumeroDeProductos( $cartItemData ) {
	wc_empty_cart();

	return $cartItemData;
}
add_filter( 'woocommerce_add_cart_item_data', 'DRCP_limitarNumeroDeProductos', 10, 1 );