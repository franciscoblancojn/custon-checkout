<?php

/**
 * Template for widget Elementor for CUestionario
 */


$value = get_option('input_custionario_settings');
$value = json_decode($value, true);
$totalPasos = 0;
$pasos_data_array = [];
$questions = [];
foreach ($value as $key => $question) {
    if ($key % 5 != 0 || $key == 0) {
        array_push($questions, $question);
        $pasos_data_array[$totalPasos] = $questions;
    } else {
        $questions = [];
        $totalPasos++;
        array_push($questions, $question);
        $pasos_data_array[$totalPasos] = $questions;
    }
}

?>
<p id="cuestionario_error">*Recuerda completar todos los campos</p>
<form action="<?= plugin_dir_url(__FILE__) ?>../processor.php" method="POST" id="form_cuestionario">
    <?php foreach ($pasos_data_array as $key_paso => $paso) : ?>
        <div class="paso_container" id="paso-<?= $key_paso + 1 ?>">
            <label class="btn_colla" for="content_collase_<?= $key_paso + 1 ?>">
                Paso <?= $key_paso + 1 ?>
            </label>

            <div class="questions_container">
                <?php foreach ($paso as $key_question => $question) :  ?>

                    <div class="question_styles_re" id="question_<?= $key_question + 1 ?>">
                        <h4 class="title">
                            <?= $question['question'] ?>
                        </h4>
                        <select id="option_<?= $i ?>" name="question[]" required class="select">
                            <option value="" selected="true" disabled="disabled">Seleccione</option>
                            <option value="0">AUSENTES</option>
                            <option value="1">OCASIONALES y LEVES</option>
                            <option value="2">OCASIONALES pero SEVEROS</option>
                            <option value="3">FRECUENTES y LEVES</option>
                            <option value="4">FRECUENTES y SEVEROS</option>
                        </select>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endforeach; ?>

    <div class="content_collase last-paso paso_container" id="paso-<?= $totalPasos + 2 ?>">
        <label class="btn_colla" for="content_collase_validation">
            Paso <?= $totalPasos + 2 ?>
        </label>
        <div class="last-select" id="validation 3">
            <h4 class="title">
                Tienes una condición autoinmune
            </h4>
            <select name="autoinmune" id="autoinmune" required class="select">
                <option value="" selected="true" disabled="disabled">Seleccione</option>
                <option value="no">No</option>
                <option value="tiroiditis autoinmune o de Hashimoto">Tiroiditis autoinmune o de Hashimoto</option>
                <option value="enfermedad de Graves">Enfermedad de Graves</option>
                <option value="Artritis reumatoide">Artritis reumatoide</option>
                <option value="Colitis ulcerativa">Colitis ulcerativa</option>
                <option value="síndrome de Sjogren">Síndrome de Sjogren</option>
                <option value="Lupus eritematoso sistémico">Lupus eritematoso sistémico</option>
                <option value="Vitíligo">Vitíligo</option>
                <option value="Morbus Crohn">Morbus Crohn</option>
                <option value="otro">Otro</option>
            </select>
        </div>
        <div class="check-form-er" id="validation 2">
            <label for="tracto_digestivo" class="title">
                La mayoría de tus síntomas se concentran en el tracto digestivo
            </label>
            <input type="checkbox" id="tracto_digestivo" name="tracto_digestivo">
        </div>
        <div class="check-form-er" id="validation 4">
            <label for="hipotiroidismo_sobrepeso" class="title">
                Tienes hipotiroidismo o tienes problemas de sobrepeso y te cuesta perderlo con dietas convencionales
            </label>
            <input type="checkbox" id="hipotiroidismo_sobrepeso" name="hipotiroidismo_sobrepeso">
        </div>
    </div>
    <div class="buttons-navigation-container">
        <button class="navigation-cuestionario-button" id="prev_question_button">Anterior</button>
        <button class="navigation-cuestionario-button" id="next_question_button">Siguiente</button>
    </div>
    <input type="submit" class="submit " id="submit-cuestionario">
</form>
<style>
    /**ajustes collase */
    .content_collase {
        overflow: hidden;
        position: relative;

    }

    .checkbox_colla {
        display: none;
    }

    .checkbox_colla:not(:checked)+.content_collase {
        height: 0;
        padding: 0 !important;
        margin: 0 !important;
        border: 0 !important;
    }

    .btn_colla {
        cursor: pointer;
        display: block;
        width: 100%;
    }

    .question_styles_re {
        display: flex;
        justify-content: space-between;
        position: relative;
        flex-wrap: wrap;
    }

    .question_styles_re .title {
        display: inline-block;
    }

    .buttons-navigation-container {
        display: flex;
        justify-content: space-around;
        align-items: center;
    }

    .question_styles_re select {
        width: 380px;
    }



    .question_styles_re:after {
        content: '';
        position: absolute;
        width: 100%;
        height: 1px;
        bottom: 0;
        background-color: #c4c4c4;
        margin: 0 auto;
        left: 0;
        right: 0;
    }

    .question_styles_re:last-child:after {
        content: none;
    }

    .last-paso div {
        border-bottom: 1px solid  #c4c4c4;
    }

    .last-select {
        display: flex;
        align-items: center;
        justify-content: space-between;
    }

    .last-select #autoinmune {
        width: 380px;
    }

    .check-form-er {
        display: flex;
        align-items: center;
        justify-content: space-between;
    }

    #cuestionario_error {
        color: #cc3366;

    }

    .check-form-er .title {
        max-width: 800px;
    }
    .buttons-navigation-container{
        margin-top: 20px;
    }
    #next_question_button{
        display: flex;
        margin-left: auto;
        outline: none;
    }
    #prev_question_button{
        display: flex;
        margin-right: auto;
        outline: none;

    }
</style>
<script>
    var page = 1;
    const buttonNext = document.querySelector('#next_question_button')
    const buttonPrev = document.querySelector('#prev_question_button')
    const pasos = document.querySelectorAll('.paso_container')
    const lastPaso = pasos.length
    const submitButton = document.querySelector('#submit-cuestionario')
    const form = document.querySelector('#form_cuestionario')
    document.querySelector('#cuestionario_error').style.display = 'none'

    buttonNext.addEventListener('click', e => {
        if (page < lastPaso) {

            page++
            e.preventDefault()
            showPage()
            showHideButtons()


        }
    })
    buttonPrev.addEventListener('click', e => {
        if (page > 0) {

            page--
            e.preventDefault()
            showPage()
            showHideButtons()

            document.querySelector('#cuestionario_error').style.display = 'none'

        }
    })

    submitButton.addEventListener('click', () => {
        if (!form.checkValidity()) {
            document.querySelector('#cuestionario_error').style.display = 'block'
        }
    })

    function showPage() {
        pasos.forEach(val => {
            const number = Number(val.id.split('-')[1])


            if (number == page) {
                val.style.display = 'block'
            } else {
                val.style.display = 'none'

            }

        })
    }

    function showHideButtons() {
        if (page == 1) {
            buttonPrev.style.display = 'none'
        } else {
            buttonPrev.style.display = 'block'

        }
        if (page == lastPaso) {
            buttonNext.style.display = 'none'
            submitButton.style.display = 'block'

        } else {
            buttonNext.style.display = 'block'
            submitButton.style.display = 'none'


        }
    }



    showPage()
    showHideButtons()
    submitButton.style.display = 'none'
</script>
<?php

// echo "<pre>";
// var_dump($value);
// echo "</pre>";