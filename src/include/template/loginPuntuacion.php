<?php

$user_id = get_current_user_id();
if($user_id == 0){
    ?>
    <a href="/my-account/" class="content_btn_loginPuntuacion">
        <button>
            Login / Register
        </button>
    </a>
    <?php
}else{
    $p = get_user_meta($user_id , 'puntuacion', true);
    if($p == null || $p == ""){ 
        ?>
        <a href="/cuestionario/" class="content_btn_loginPuntuacion">
            <button>
                No has rellenado el cuestionario 
            </button>
        </a>
        <?php
    }else{
        ?>
        <a href="/cuestionario/" class="content_btn_loginPuntuacion">
            <button>
                Tu puntuacion es <?=$p?>, quieres actualizarlo
            </button>
        </a>
        <?php
    }
}
?>
<style>
    .content_btn_loginPuntuacion{
        display:block;
        text-align:center;
    }
    .content_btn_loginPuntuacion button{
        box-sizing: border-box;
        width: auto;
        max-width: 100%;
        white-space: initial;
        border-radius:0;
    }
</style>
<?php